#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62

typedef long long ll;

using namespace std;
bool a[100001][101]={false};
vector <ll>q;
int main()
{
    ll n , t;
 //   Test;
    cin>>t;
    while(t--)
    {
        ll sum =0;
        cin>>n;
        Rep(i , n)
        {
            ll x;
            cin>>x;
            q.push_back(x);
            sum+=x;
        }
        if(sum % 2 == 0)
        {
            sum/=2;
            Rep(i , n + 1 )
            {
                a[0][i]=true;
            }
            For(i ,1, sum+1)
            {
                a[i][0]=false;
            }
            Rep(i , sum+1)
            {
                For(j ,1, n+1)
                {
                    if(a[i][j-1] == true)
                    {
                        a[i][j]= true;
                        if(i+q[j-1] <= sum)
                        {
                            a[i + q[j-1]][j]=true;
                        }
                    }
                    else if(a[i][j] != true)
                        a[i][j]=false;

                }
            }
            if(a[sum][n])
                cout<<"YES"<<endl;
            else
                cout<<"NO"<<endl;
        }
        else
            cout<<"NO"<<endl;
        q.clear();
        Rep(i ,sum + 1)
            Rep(j ,n +1)
                a[i][j]=false;
    }
    return 0;
}
